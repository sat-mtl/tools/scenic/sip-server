# Release <project> <version>

## Summary
Should be closed when all the following tasks are resolved:

| Issue  | Merge request | Name         | Status           |
|--------|---------------|--------------|------------------|
| #issue | !request(s)   | Resolve this | ~"Ready to test" |
|        |               |              |                  |

## Checklists
All tasks are documented in the [release handbook](https://sat-mtl.gitlab.io/valorisation/internal-docs/guides/release/).

### Requirements
- [ ] [Create a milestone `<project> <version>`](https://gitlab.com/sat-mtl/tools/scenic/sip-server/-/milestones/new)
- [ ] Tag all issues planned for this release
- [ ] [Create an issue with a release overview](https://gitlab.com/sat-mtl/tools/scenic/sip-server/-/issues/new?issue[title]=Test%20and%20release%20candidate%204.x.x&issuable_template=Release%20Plan)

### Starting
- [ ] [Create the branch `candidate/x.x.x`](https://gitlab.com/sat-mtl/tools/scenic/sip-server/-/branches/new?branch_name=candidate%2F4.x.x&ref=develop)
- [ ] [Create a merge request from `candidate/x.x.x` to `master`](https://gitlab.com/sat-mtl/tools/scenic/sip-server/-/merge_requests/new?merge_request[target_branch]=master&merge_request[source_branch]=candidate/4.x.x)

### Updating
- [ ] Rebase candidate branch onto `develop`
- [ ] Update all files:
  - [ ] CHANGELOG.md
  - [ ] AUTHORS.md
  - [ ] README.md
- [ ] Update the software version in
  - [ ] package.json
  - [ ] README.md
- [ ] Make announcement in the QA channel with the information:
  - [ ] All added fixes and features
  - [ ] A link to this issue with an updated overview

### Ending
- [ ] Update all files:
  - [ ] CHANGELOG.md
  - [ ] AUTHORS.md
  - [ ] README.md
- [ ] Update the software version in
  - [ ] package.json
  - [ ] README.md
- [ ] Merge the MR from the candidate branch to `master` or `main`
- [ ] Tag the production branch with
  - [ ] the current version
  - [ ] the last CHANGELOG
- [ ]  Create a merge request from `master` to `develop`
  - [ ] Merge back every change
- [ ] Close the milestone and this issue

/assign @vlaurent
/cc @flecavalier
/milestone %"<milestone>"
/label ~"type::release candidate" ~"In development" ~"priority::high" ~"sprint::unscheduled"
