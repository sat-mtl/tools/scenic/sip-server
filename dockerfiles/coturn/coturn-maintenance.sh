#!/bin/sh
# Deletes coturn logs older than days_keep_logs days.
days_keep_logs=30

find /var/log/coturn/turn*.log -mtime +$days_keep_logs -exec rm {} \;